class LastViewedStories extends HTMLElement {

  elementWidth;
  maxHeight;

  localStoragePropertyName = 'recents';

  shadow = null;

  template = '<div><h3>Recent stories ...</h3><slot name="story-list" id="list-slot"></slot><p class="auxiliary-copy">There could be a little description here ...</p></div>';

  interval = null;

  constructor() {
    super();
    this.maxHeight = this.getAttribute('height') || '350px';
    this.elementWidth = this.getAttribute('width') || '250px';
    this.shadow = this.attachShadow({mode: 'open'});
  }

  disconnectedCallback() {
    clearInterval(this.interval);
  }

  connectedCallback() {
    this.addCurrentPageToList();
    this.interval = setInterval(this.reloadAndRerender.bind(this), 15000);
    this.shadow.appendChild(this.generateComponentStyles());
    this.shadow.appendChild(this.renderUiElement());
  }

  addCurrentPageToList() {
    const url = window.location.href;
    const pageName = document.title;
    const currentTime = new Date().toISOString();

    let newList = JSON.parse(localStorage.getItem(this.localStoragePropertyName))
      .filter(pageRec => pageRec.url !== url)
    newList.unshift({id: (Math.round(Math.random()*10000)), title: pageName, url, date: currentTime});
    localStorage.setItem(this.localStoragePropertyName, JSON.stringify(newList));
  }

  renderUiElement() {
    let previousPagesViewedListData = this.loadStoredList();
    const storyListContainer = this.renderContainer();

    let listContainer = this.getSlotByName(storyListContainer, 'story-list');
    listContainer.appendChild(this.renderLinksFor(previousPagesViewedListData));
    return storyListContainer;
  }

  loadStoredList() {
    const recentStoriesString = localStorage.getItem(this.localStoragePropertyName) || '[]';
    const sortedList = JSON.parse(recentStoriesString).sort(this.sortByDate);
    return sortedList;
    return this.deduplicateListOfLinks(sortedList);
  }

  deduplicateListOfLinks(linkList) {
    return linkList.reduce((acc, curr) => {
      if (!acc.find(entry => entry.url == curr.url)) acc.unshift(curr);
      return acc;
    }, []);
  }

  sortByDate(a, b) {
    if (a.date < b.date) return 1;
    else if (a.date > b.date) return -1;
    else return 0
  }

  renderContainer() {
    const container = document.createElement('section');
    container.setAttribute('class', 'recent-articles__box');
    container.innerHTML = this.template;
    return container;
  }

  getSlotByName(html, slotNameAttributeValue) {
    return html.getElementsByTagName('slot').namedItem(slotNameAttributeValue);
  }

  renderLinksFor(storyList) {
    const listRoot = document.createElement('ol');
    listRoot.setAttribute('id', 'list-root-element');
    for (let storyDetails of storyList) {
      this.appendListItemChild(listRoot, storyDetails);
    }
    return listRoot;
  }

  appendListItemChild(listRootElement, storyData) {
    const storyItem = document.createElement('li');

    storyItem.appendChild(this.renderStoryLink(storyData));
    if (storyData.title.length > 30) storyItem.appendChild(this.renderTooltip(storyData.title));
    storyItem.appendChild(this.renderDate(storyData));

    listRootElement.appendChild(storyItem);
  }

  renderStoryLink(storyData) {
    const storyLink = document.createElement('a');
    storyLink.setAttribute('class', 'truncate');
    storyLink.appendChild(document.createTextNode(storyData.title));
    storyLink.setAttribute('href', storyData.url);
    storyLink.setAttribute('alt', storyData.title);
    return storyLink;
  }

  renderTooltip(copyForTooltip) {
    const fullStoryTitlePopup = document.createElement('div');
    fullStoryTitlePopup.setAttribute('class', 'full-title-hover');
    fullStoryTitlePopup.appendChild(document.createTextNode(copyForTooltip));
    return fullStoryTitlePopup;
  }

  renderDate(storyData) {
    const dateElapsedText = this.generateElapsedTimeCopy(storyData.date);
    const dateCopy = document.createElement('p')
    dateCopy.setAttribute('class', 'item-viewed-date');
    dateCopy.appendChild(document.createTextNode(dateElapsedText));
    return dateCopy;
  }

  generateElapsedTimeCopy(storyViewedDateStr) {
    const storyViewedDate = new Date(storyViewedDateStr);
    const nowDate = Date.now();
    const dateDiffInSecs = Math.floor((nowDate - storyViewedDate) / 1000);
    const dateDiffInMinutes = Math.round(dateDiffInSecs / 60);
    const dateDiffInHours = Math.round(dateDiffInMinutes / 60);
    const viewSecondsFromEndOfToday = (this.getEndOfToday() - storyViewedDate) / 1000;
    const daysAgo = Math.round((nowDate - storyViewedDate) / 86400000);

    // if less than 1 hour, show minutes
    // if less than 4 hours, show "x hours ago"
    // if less than difference between view time and EOD, show "today"
    // if less than 7 days, show "this week"
    if (dateDiffInSecs < 60) return `just now ...`;
    else if (dateDiffInMinutes < 60) return `${ dateDiffInMinutes } minute${ dateDiffInMinutes == 1 ? '' : 's' } ago`;
    else if (dateDiffInHours < 8) return `${ dateDiffInHours } hour${ dateDiffInHours < 2 ? '' : 's' } ago`;
    else if (viewSecondsFromEndOfToday < 86400) return `today`;
    else if (viewSecondsFromEndOfToday > 86400 && viewSecondsFromEndOfToday < 172800) return 'yesterday';
    else if (daysAgo < 8) return `this week`;
    else return storyViewedDate.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      })
  }

  getEndOfToday() {
    const current = new Date();
    const dateStr = `${current.getFullYear()}-${current.getMonth() + 1}-${current.getDate()}T23:59:59`;
    return new Date(dateStr);
  }

  reloadAndRerender() {
    const shadowRoot = this.shadowRoot;
    const listOffset = this.shadowRoot.getElementById('list-root-element').scrollTop;
    const slotElement = shadowRoot.getElementById('list-slot');
    const updatedEntries = this.renderLinksFor(this.loadStoredList());
    slotElement.innerHTML = '';
    slotElement.appendChild(updatedEntries);
    slotElement.childNodes[0].scrollTop = listOffset;
  }

  generateComponentStyles() {
    const styleElement = document.createElement('style');
    styleElement.setAttribute('type', 'text/css');

    styleElement.textContent = `
.recent-articles__box {
border: 1px solid #555;
border-radius:7px;
padding:0px;
min-width:${this.elementWidth};
}

h3 {
font-size: 15px;
border-bottom: 1px solid #555;
position: relative;
padding: 10px 10px;
margin: 0;
}

li {
list-style-type: none;
position:relative;
}

.truncate {
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.item-viewed-date {
    margin: 0 0 0.8em;
    font-style: italic;
    font-size: 0.9em;
}

a {
text-decoration: none;
color: #a3122a;
display:inline-block;
}

.auxiliary-copy {
font-size:10px;
text-align:center;
}

ol {
max-height:${this.maxHeight};
overflow-y: scroll;
padding:0 13px;
}

.full-title-hover {
position: absolute;
visibility: hidden;
padding:0.2em;
background: #ccc;
border: 1px solid #555;
border-radius:4px;
z-index:1;
}

li:hover div.full-title-hover {
visibility: visible;
}
`;
    return styleElement;
  }
}

customElements.define('last-viewed', LastViewedStories);
